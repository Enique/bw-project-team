import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.Serializable;
public class Example implements Serializable{
   public static double getDays(String date1,String date2){
	 SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
	 String dateBeforeString = date1;
	 String dateAfterString = date2;
	 double daysBetween=0;
	 try {
	       Date dateBefore = myFormat.parse(dateBeforeString);
	       Date dateAfter = myFormat.parse(dateAfterString);
	       long difference = dateAfter.getTime() - dateBefore.getTime();
	       daysBetween = (difference / (1000*60*60*24));
               /* You can also convert the milliseconds to days using this method
                * float daysBetween = 
                *         TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS)
                */
	    
	 }
				
			catch (Exception e) {
	       e.printStackTrace();
	    }
		return daysBetween;
   }
	
}